package me.forumat.voteapi.example;

import me.forumat.voteapi.VoteAPI;
import me.forumat.voteapi.api.ItemBuilder;
import me.forumat.voteapi.api.VoteElement;
import me.forumat.voteapi.api.Voting;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DayVoteCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if (commandSender instanceof Player) {
            if(Voting.isVoteStarted("DAY-VOTE")){
                commandSender.sendMessage("§cEs startet bereits eine Abstimmung!");
                return true;
            }
            Voting voting = new Voting("DAY-VOTE", "§7Du hast erfolgreich für §e%VOTE% §7gevoted!", 200, 9, VoteAPI.getInstance(), (voteElement) -> {
                Bukkit.broadcastMessage("§7Der Gewinner ist: " + voteElement.getDisplay().getItemMeta().getDisplayName());
                if (voteElement.getName().equalsIgnoreCase("JA")) {
                    Bukkit.getWorlds().forEach(world -> world.setTime(600));
                }
            }, new VoteElement(ItemBuilder.getItemStack("§a§lJa!", Material.WATCH), "JA", 3),
                    new VoteElement(ItemBuilder.getItemStack("§c§lNein!", Material.BARRIER), "NEIN", 5));
            voting.startTimer();

            Bukkit.getOnlinePlayers().forEach(player -> player.openInventory(voting.getInventory()));

        }

        return false;
    }
}
