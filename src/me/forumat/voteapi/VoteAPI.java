package me.forumat.voteapi;

import me.forumat.voteapi.api.VoteInventoryClickListener;
import me.forumat.voteapi.example.DayVoteCommand;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class VoteAPI extends JavaPlugin {

    private static VoteAPI instance;

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getPluginManager().registerEvents(new VoteInventoryClickListener(), this);

        getCommand("dayvote").setExecutor(new DayVoteCommand());
    }


    public static VoteAPI getInstance() {
        return instance;
    }
}
