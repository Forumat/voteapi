package me.forumat.voteapi.api;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.Consumer;

public class Voting {

    private String name, message;
    private int time;
    private JavaPlugin javaPlugin;
    private Consumer<VoteElement> onFinish;
    private VoteElement[] voteElements;

    private Inventory inventory;
    private List<UUID> voted;
    private Map<ItemStack, VoteElement> elementMap;

    private static final Map<String, Voting> currentVotings = new HashMap<>();
    private static final Map<Inventory, Voting> inventoryVotingMap = new HashMap<>();


    public Voting(String name, String message, int time, int inventorySize, JavaPlugin javaPlugin, Consumer<VoteElement> onFinish, VoteElement... voteElements) {
        this.name = name;
        this.javaPlugin = javaPlugin;
        this.message = message;
        this.time = time;
        this.onFinish = onFinish;
        this.voteElements = voteElements;
        voted = new ArrayList<>();
        elementMap = new HashMap<>();
        inventory = Bukkit.createInventory(null, inventorySize, name);
        for (VoteElement voteElement : voteElements) {
            inventory.setItem(voteElement.getSlot(), voteElement.getDisplay());
            elementMap.put(voteElement.getDisplay(), voteElement);
        }

        inventoryVotingMap.put(inventory, this);

    }


    public static boolean isVoteStarted(String name){
        return getCurrentVotings().containsKey(name);
    }

    public void startTimer(){
        currentVotings.put(name, this);
        Bukkit.getScheduler().runTaskLaterAsynchronously(javaPlugin, () -> {
            VoteElement winner = getWinner();
            winner.setHasWon(true);
            onFinish.accept(winner);
            currentVotings.remove(name);
            inventoryVotingMap.remove(inventory);
        }, time);
    }

    public VoteElement getWinner(){
        List<VoteElement> voteElementList = Arrays.asList(voteElements);
        voteElementList.forEach(voteElement -> System.out.println(voteElement.getName() + " : " + voteElement.getVotes()));
        voteElementList.sort(Comparator.comparingInt(VoteElement::getVotes));
        Collections.reverse(voteElementList);
        return voteElementList.get(0);
    }


    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    public int getTime() {
        return time;
    }

    public JavaPlugin getJavaPlugin() {
        return javaPlugin;
    }

    public Consumer<VoteElement> getOnFinish() {
        return onFinish;
    }

    public VoteElement[] getVoteElements() {
        return voteElements;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public List<UUID> getVoted() {
        return voted;
    }

    public Map<ItemStack, VoteElement> getElementMap() {
        return elementMap;
    }

    public static Map<String, Voting> getCurrentVotings() {
        return currentVotings;
    }

    public static Map<Inventory, Voting> getInventoryVotingMap() {
        return inventoryVotingMap;
    }
}
