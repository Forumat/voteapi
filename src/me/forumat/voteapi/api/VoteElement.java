package me.forumat.voteapi.api;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class VoteElement {

    private ItemStack display;
    private String name;
    private int slot;
    private int votes;
    private boolean hasWon;
    private static Map<String, VoteElement> elementMap = new HashMap<>();;

    public VoteElement(ItemStack display, String name, int slot) {
        this.display = display;
        this.name = name;
        this.slot = slot;
        hasWon = false;
        votes = 0;
        elementMap.put(name, this);
    }


    public static Map<String, VoteElement> getElementMap() {
        return elementMap;
    }

    public String getName() {
        return name;
    }

    public void addVote(){
        votes += 1;
    }

    public void setHasWon(boolean hasWon) {
        this.hasWon = hasWon;
    }

    public ItemStack getDisplay() {
        return display;
    }

    public int getSlot() {
        return slot;
    }

    public int getVotes() {
        return votes;
    }

    public boolean isHasWon() {
        return hasWon;
    }
}
