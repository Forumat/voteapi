package me.forumat.voteapi.api;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class VoteInventoryClickListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (Voting.getInventoryVotingMap().containsKey(event.getClickedInventory())) {
            Voting voting = Voting.getInventoryVotingMap().get(event.getClickedInventory());
            if(event.getCurrentItem().getType() != Material.AIR){
                VoteElement voteElement = voting.getElementMap().get(event.getCurrentItem());
                if(!voting.getVoted().contains(event.getWhoClicked().getUniqueId())) {
                    voteElement.addVote();
                    event.setCancelled(true);
                    event.getWhoClicked().closeInventory();
                    voting.getVoted().add(event.getWhoClicked().getUniqueId());
                    event.getWhoClicked().sendMessage(voting.getMessage().replace("%VOTE%", voteElement.getDisplay().getItemMeta().getDisplayName()));
                }else{
                    event.getWhoClicked().sendMessage("§cDu hast schon gevoted!");
                }
            }
        }
    }

}
